//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let numberSet = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    let positiveOdd = [];
    let ngeativeEven = [];


    for (i = 0; i < numberSet.length;i++)
        if (numberSet[i] > 0 && numberSet[i] % 2 == 1)
        {
            positiveOdd.push(numberSet[i])
        }
        else if(numberSet[i] < 0 && numberSet[i] % 2 == 0)
        {
            ngeativeEven.push(numberSet[i])
        };

    output += "Positive Odd : "
    for (var i =0; i < positiveOdd.length;i++)
    {
        output += positiveOdd[i];
        if (positiveOdd.length != i+1)
        {
            output += ", "
        }
    };

    output += '\n';

    output += "Negative Even : "
    for (var i =0; i < ngeativeEven.length;i++)
    {
        output += ngeativeEven[i];
        if (ngeativeEven.length != i+1)
        {
            output += ", "
        }
    };



    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here

    // Creating variables corresponding to each number of the dice and assign them t zero
    let number1 = 0;
    let number2 = 0;
    let number3 = 0;
    let number4 = 0;
    let number5 = 0;
    let number6 = 0;

    for (i = 0; i < 60000; i++)
    {
        diceNumber = Math.floor((Math.random() * 6) + 1);

        if (diceNumber == 6)
        {
            number6 += 1
        }

        else if(diceNumber == 5)
        {
            number5 += 1
        }
        else if(diceNumber == 4)
        {
            number4 += 1
        }
        else if(diceNumber == 3)
        {
            number3 += 1
        }
        else if(diceNumber == 2)
        {
            number2 += 1
        }
        else
        {
            number1 += 1
        }

    }; 
    
    output += "Frequency of die rolls\n" 
    
    output += `1: ${number1} \n`
    output += `2: ${number2} \n`
    output += `3: ${number3} \n`
    output += `4: ${number4} \n`
    output += `5: ${number5} \n`
    output += `6: ${number6} \n`
    

    let outPutArea = document.getElementById("outputArea2") 
    
    outPutArea.innerText = output
}

function question3(){
    let output = "" 
    
    //Question 3 here

    // Creating the array with seven elements
    let diceArray = [0,0,0,0,0,0,0];

    // Using a loop to turn the dice 60,000 times
    for (i = 0; i < 60000; i++)
    {
        diceNumber = Math.floor((Math.random() * 6) + 1);

    // Updating the 'diceArray' list according to the number turned
        diceArray[diceNumber] += 1 
    };
    
    output += "Frequency of die rolls\n" 

    // Using a loop to make 'output' as required
    for(i = 1; i <= 6; i++)
    {
        output += `${i}: ${diceArray[i]} \n`
    };
    
    // Printing the output in to the HTML document
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    
    let output = "" 
    
    //Question 4 here

    // Naming and assgining the dieRolls object
    var dieRolls = {
        Frequencies: {
        1:0,
        2:0,
        3:0,
        4:0,
        5:0,
        6:0,
        },
        Total:60000,
        Exceptions: ""
        }
    
    // Using a loop to turn the dice 60,000 times
    for (i = 0; i < 60000; i++)
    {
        dieNumber = Math.floor((Math.random() * 6) + 1);

        dieRolls.Frequencies[dieNumber] += 1
    };

    // Iterating through the object 'Frequencies' in 'dieRolls'
    for(prop in dieRolls.Frequencies)
    {
    // Die faces that occurred with a frequency that is more than 1% over or under its expected value
        if (Math.abs(dieRolls.Frequencies[prop]-10000) > 100)
        {
                dieRolls.Exceptions += `${prop} `
        };
    };

    // Trimming the last whitespace in the 'dieRolls.Exceptions'
    dieRolls.Exceptions = dieRolls.Exceptions.trim();

    output += "Frequency of dice rolls\n"
    output += `Total rolls: ${dieRolls.Total}\n`
    output +=  "Frequencies:\n"
    for(i = 1; i <= 6; i++)
    {
        output += `${i}: ${dieRolls.Frequencies[i]} \n`
    };

    output += `Exceptions: ${dieRolls.Exceptions}`


    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 

    let person = {
        name: "Jane",
        income: 127050
        }

    let personIncome = person.income;
    if (personIncome >=  180001)
    {
        tax = 54097 + 0.45*(personIncome-180000)
    }

    else if (personIncome <=  180000 && personIncome >= 90001)
    {
        tax = 20797 + 0.37*(personIncome-90000)
    }

    else if (personIncome <=  90000 && personIncome >= 37001)
    {
        tax = 3572 + 0.325*(personIncome-37000)
    }

    else if (personIncome <=  37000 && personIncome >= 18201)
    {
        tax = 20797 + 0.19*(personIncome-90000)
    }
    else
    {
        tax = 0
    };

    // Rounding and formating the 'tax' to dollars with just 2 decimal places.
    taxRounded = tax.toLocaleString('en-US', {style: 'currency',currency: 'USD', minimumFractionDigits: 2})


    output += `${person.name}'s income is : ${personIncome}, and her tax owed is: ${taxRounded}.`

    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}