// Copying the variables from the practical
let animalString = "cameldogcatlizard";
let andString = " and ";

// Slicing and concatanating strings as required using the substring method
let phrase1 = animalString.substring(8,11) + andString + animalString.substring(5,8);

// Slicing and concatanating strings as required using the substr method
let phrase2 = animalString.substr(8,3) + andString + animalString.substr(5,3);

// Printing the results
console.log(phrase1 + '\n' + phrase2);