// Defining the radius 
const radius = 4;

// Defining the circumference 
let circum = 2*Math.PI*radius;

//Printing requird results on the console
console.log(circum.toFixed(2));