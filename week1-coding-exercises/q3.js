// Assigning the object person and its properties
let person = {
    firstName: "Kanye",
    lastName: "West",
    birthDate: "8 June 1977",
    annualIncome: 150000000.00
};

// Concatanation of the required string 
outputMssg = `${person.firstName} ${person.lastName} was born on ${person.birthDate} and has an annual income of $${Math.round(person.annualIncome)}`;

// Printinh the message on the console
console.log(outputMssg);