var outputAreaRef = document.getElementById("outputArea1");
var output = "";
function flexible(fOperation, operand1, operand2)
{
var result = fOperation(operand1, operand2);
return result;
}

function Sum(num1, num2)
{
    return num1 + num2
}

function Multiply(num1, num2)
{
    return num1 * num2
}

output += flexible(Sum, 3, 5) + "<br/>";
output += flexible(Multiply, 3, 5) + "<br/>";
outputAreaRef.innerHTML = output;

/*
Question 2 Part A

Get the object into the function through a parameter
create a variable output in the function
Loop through the object using for loop
    mutate the variable output with the property and its value
return the output value 
*/


// Question 2 Part b

var outputAreaRef = document.getElementById("outputArea2");
var output = "";

function objectToHTML(Object)
{   
    let output = '';
    for(let prop in Object)
    {
        output +=`${String(prop)}: ${Object[prop]}` + "<br/>";
    }

    return output
}

var testObj2 = {
    name: "Bob",
    age: 32,
    married: true,
    children: ["Jack", "Jill"]
    };

output = objectToHTML(testObj2);
outputAreaRef.innerHTML = output;


/*
Question 3 Part A

Assign two variables in the global scope minVal and maxVal to integerArray[0]
loop through the array
    check if the particular digit is greater than maxVal
        is so set maxVal to that value
    if difit is less than minVal, assign that to minVAl

*/


// Question 3 Part b

var outputAreaRef = document.getElementById("outputArea3");
var output = "";

var maxVal;
var minVal;

function extremeValues(integerArray)
{   
    maxVal = integerArray[0];
    minVal = integerArray[0];
    
    for(i = 0; i < integerArray.length; i++)
    {
        if(integerArray[i]>maxVal)
        {
            maxVal = integerArray[i];
        }
        else if(integerArray[i]<minVal)
        {
            minVal = integerArray[i];
        }
    }
}

var values = [4, 3, 6, 12, 1, 3, 8];
extremeValues(values);

output += maxVal + "<br/>"
output += minVal + "<br/>"
console.log(maxVal);

outputAreaRef.innerHTML = output;